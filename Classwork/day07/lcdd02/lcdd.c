#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>


#define MAXLEN 25 
static char my_buf[MAXLEN];

static dev_t devno;
static int major_no = 250;
static struct class *pclass;
static struct cdev my_dev;

static int char_open(struct inode *pinode, struct file *pfile) {
	printk(KERN_INFO "char_open() called.\n");
	return 0;
}
static int char_close(struct inode *pinode, struct file *pfile) {
	printk(KERN_INFO "char_close() called.\n");
	return 0;
}
static ssize_t char_read(struct file *pfile, char __user *pbuf, size_t len, loff_t *pfpos) {
	int bytes_available,bytes_to_read,nbytes;
    //printk(KERN_INFO "char_write() called.\n");
    
    bytes_available = MAXLEN - *pfpos;
    bytes_to_read = len < bytes_available ? len : bytes_available;
    if(bytes_to_read == 0)
    {
        printk("char_read : Buffer reached at end.\n");
        return 0;
    }
    nbytes = bytes_to_read - copy_to_user(pbuf, my_buf + *pfpos, bytes_to_read);
    *pfpos = *pfpos + nbytes;
    printk("char_read : Number of bytes read %d.\n",nbytes);
	return nbytes;
}
static ssize_t char_write(struct file *pfile, const char __user *pbuf, size_t len, loff_t *pfpos) {
	int bytes_available,bytes_to_write,nbytes;
    //printk(KERN_INFO "char_write() called.\n");
    
    bytes_available = MAXLEN - *pfpos;
    bytes_to_write = len < bytes_available ? len : bytes_available;
    if(bytes_to_write == 0)
    {
        printk("char_write : Buffer out of space.\n");
        return -ENOSPC;
    }
    nbytes = bytes_to_write - copy_from_user(my_buf + *pfpos, pbuf , bytes_to_write );
    *pfpos = *pfpos + nbytes;
    printk("char_write : Number of bytes written %d.\n",nbytes);
	return nbytes;
}
static loff_t char_lseek(struct file *pfile, loff_t offset , int origin)
{
    loff_t newpos;
    switch(origin)
    {
        case SEEK_SET:
            newpos = 0 + offset;
            break;
        case SEEK_CUR:
            newpos = pfile->f_pos + offset;
            break;
        case SEEK_END:
            newpos = MAXLEN + offset;
            break;
        default:
            printk(KERN_INFO "invalid origin in char_lseek().\n");
            break;
    }
    if(newpos < 0)
        newpos = 0;
    if(newpos > MAXLEN)
        newpos = MAXLEN;
    printk("New position : %lld.\n",newpos);
    pfile->f_pos = newpos;
    return newpos;


}

static struct file_operations fops={
    /* data */
    .owner = THIS_MODULE,
    .open = char_open,
    .release = char_close,
    .read = char_read,
    .write = char_write,
    .llseek = char_lseek
};


static int __init my_char_init(void)
{
    int minor_no,ret;
    struct device *pdevice;
    
    printk(KERN_INFO "%s: Init char device.\n",THIS_MODULE->name);
    // allocation of device number
    devno = MKDEV(major_no,0);
    ret=alloc_chrdev_region(&devno,0,1,"my_char");
    if(ret!=0)
    {
        printk(KERN_INFO "%s: alloc_chrdev_region() failed.\n",THIS_MODULE->name);
        return -1;
    }
    major_no=MAJOR(devno);
    minor_no=MINOR(devno);
    printk(KERN_INFO "%s: char device major number  :   %d.\n",THIS_MODULE->name,major_no);
    printk(KERN_INFO "%s: char device minor number  :   %d.\n",THIS_MODULE->name,minor_no);

    // create class and create device file
    pclass=class_create(THIS_MODULE,"char_class");
    if(IS_ERR(pclass))
    {
        printk(KERN_INFO "%s: class_create() failed.\n",THIS_MODULE->name);
        unregister_chrdev_region(devno,1);
        return -1;
    }
        printk(KERN_INFO "%s: class_create() class has been created.\n",THIS_MODULE->name);
    
    pdevice = device_create(pclass,NULL,devno,NULL,"char_dev");
    if(IS_ERR(pdevice))
    {
        printk(KERN_INFO "%s: device_create() failed.\n",THIS_MODULE->name);
        class_destroy(pclass);    
        unregister_chrdev_region(devno,1);
        return -1;
    }
    printk(KERN_INFO "%s: device_create() device for class has been created.\n",THIS_MODULE->name);

    //cdev initialization and adding it to the kernel
    cdev_init(&my_dev,&fops);
    ret = cdev_add(&my_dev,devno,1);
    if(ret!=0)
    {
        printk(KERN_INFO "%s: cdev_add() failed.\n",THIS_MODULE->name);
        device_destroy(pclass,devno);    
        class_destroy(pclass);    
        unregister_chrdev_region(devno,1);
        return -1;
    }
    printk(KERN_INFO "%s: char_dev has been added.\n",THIS_MODULE->name);
    return 0;
}

static void __exit my_char_exit(void)
{
    printk(KERN_INFO "%s: Exit char device.\n",THIS_MODULE->name);
    cdev_del(&my_dev);
    printk(KERN_INFO "%s: device has been deleted.\n",THIS_MODULE->name);
    device_destroy(pclass,devno);
    printk(KERN_INFO "%s: destroy device file.\n",THIS_MODULE->name);
    class_destroy(pclass);
    printk(KERN_INFO "%s: destroy device class.\n",THIS_MODULE->name);
    unregister_chrdev_region(devno,1);
    printk(KERN_INFO "%s: char device de-allocated.\n",THIS_MODULE->name);
}

module_init(my_char_init);
module_exit(my_char_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin  <abcd@gmail.com>");
MODULE_DESCRIPTION("MY FIRST CHAR DEVICE");

