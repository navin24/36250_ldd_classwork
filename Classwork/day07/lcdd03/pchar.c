#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>


#define MAXLEN 32
#define dev_count 4

typedef struct pchar_device{
    char my_buf[MAXLEN];
    struct cdev my_dev;
}pchar_device_t;

static dev_t devno;
static int major_no = 250;
static struct class *pclass;
static  pchar_device_t pchar_array[dev_count];

static int char_open(struct inode *pinode, struct file *pfile) {
	
    pchar_device_t *dev = container_of(pinode->i_cdev,pchar_device_t,my_dev);
    pfile->private_data = dev;
    printk(KERN_INFO "char_open() called.\n");
	return 0;
}
static int char_close(struct inode *pinode, struct file *pfile) {
	printk(KERN_INFO "char_close() called.\n");
	return 0;
}
static ssize_t char_read(struct file *pfile, char __user *pbuf, size_t len, loff_t *pfpos) {
   
	int bytes_available,bytes_to_read,nbytes;
    //printk(KERN_INFO "char_write() called.\n");
     pchar_device_t *dev = (pchar_device_t *)pfile->private_data;
    bytes_available = MAXLEN - *pfpos;
    bytes_to_read = len < bytes_available ? len : bytes_available;
    if(bytes_to_read == 0)
    {
        printk("char_read : Buffer reached at end.\n");
        return 0;
    }
    nbytes = bytes_to_read - copy_to_user(pbuf, dev->my_buf + *pfpos, bytes_to_read);
    *pfpos = *pfpos + nbytes;
    printk("char_read : Number of bytes read %d.\n",nbytes);
	return nbytes;
    
}
static ssize_t char_write(struct file *pfile, const char __user *pbuf, size_t len, loff_t *pfpos) {
    
	int bytes_available,bytes_to_write,nbytes;
    pchar_device_t *dev = (pchar_device_t *)pfile->private_data;
    //printk(KERN_INFO "char_write() called.\n");
    
    bytes_available = MAXLEN - *pfpos;
    bytes_to_write = len < bytes_available ? len : bytes_available;
    if(bytes_to_write == 0)
    {
        printk("char_write : Buffer out of space.\n");
        return -ENOSPC;
    }
    nbytes = bytes_to_write - copy_from_user(dev->my_buf + *pfpos, pbuf , bytes_to_write );
    *pfpos = *pfpos + nbytes;
    printk("char_write : Number of bytes written %d.\n",nbytes);
	return nbytes;
}
static loff_t char_lseek(struct file *pfile, loff_t offset , int origin)
{
    loff_t newpos;
    switch(origin)
    {
        case SEEK_SET:
            newpos = 0 + offset;
            break;
        case SEEK_CUR:
            newpos = pfile->f_pos + offset;
            break;
        case SEEK_END:
            newpos = MAXLEN + offset;
            break;
        default:
            printk(KERN_INFO "invalid origin in char_lseek().\n");
            break;
    }
    if(newpos > MAXLEN)
        newpos = MAXLEN;
    if(newpos < 0)
        newpos = 0;
     
    printk("New position : %lld.\n",newpos);
    pfile->f_pos = newpos;
    return newpos;


}

static struct file_operations fops={
    /* data */
    .owner = THIS_MODULE,
    .open = char_open,
    .release = char_close,
    .read = char_read,
    .write = char_write,
    .llseek = char_lseek
};


static int __init my_char_init(void)
{
    int minor_no,ret,i;
    struct device *pdevice;
    dev_t l_devno;    
    printk(KERN_INFO "%s: Init char device.\n",THIS_MODULE->name);
    // allocation of device number
    devno = MKDEV(major_no,0);
    ret=alloc_chrdev_region(&devno,0,dev_count,"my_char");
    if(ret!=0)
    {
        printk(KERN_INFO "%s: alloc_chrdev_region() failed.\n",THIS_MODULE->name);
        return -1;
    }
    major_no=MAJOR(devno);
    minor_no=MINOR(devno);
    printk(KERN_INFO "%s: char device major number  :   %d.\n",THIS_MODULE->name,major_no);
    printk(KERN_INFO "%s: char device minor number  :   %d.\n",THIS_MODULE->name,minor_no);

    // create class and create device file
    pclass=class_create(THIS_MODULE,"char_class");
    if(IS_ERR(pclass))
    {
        printk(KERN_INFO "%s: class_create() failed.\n",THIS_MODULE->name);
        unregister_chrdev_region(devno,dev_count);
        return -1;
    }
        printk(KERN_INFO "%s: class_create() class has been created.\n",THIS_MODULE->name);
    for(i=0;i<dev_count;i++)
    {
        l_devno = MKDEV(major_no,i);
        pdevice = device_create(pclass,NULL,l_devno,NULL,"char_dev%d",i);
        if(IS_ERR(pdevice))
        {
            printk(KERN_INFO "%s: device_create() failed.\n",THIS_MODULE->name);
            for(i=i-1;i>=0;i--)
            {
                l_devno = MKDEV(major_no,i);
                device_destroy(pclass,l_devno);            
            }
            class_destroy(pclass);    
            unregister_chrdev_region(devno,dev_count);
            return -1;
        }
        printk(KERN_INFO "%s: device_create() device file %d has been created.\n",THIS_MODULE->name,i);
    }
    //cdev initialization and adding it to the kernel
    for(i=0;i<dev_count;i++)
    {
        cdev_init(&pchar_array[i].my_dev,&fops);
        l_devno = MKDEV(major_no,i);
        ret = cdev_add(&pchar_array[i].my_dev,l_devno,1);
        if(ret!=0)
        {
            printk(KERN_INFO "%s: cdev_add() failed.\n",THIS_MODULE->name);
            for(i=i-1;i>0;i--)
            {
                cdev_del(&pchar_array[i].my_dev);
            }
            for(i=dev_count-1;i>=0;i--)
            {
                l_devno = MKDEV(major_no,i);
                device_destroy(pclass,l_devno);            
            }
            class_destroy(pclass);    
            unregister_chrdev_region(devno,dev_count);
            return -1;
        }
        printk(KERN_INFO "%s: char_dev%d has been added.\n",THIS_MODULE->name,i);
    }
    return 0;
}

static void __exit my_char_exit(void)
{
    dev_t l_devno;
    int i;
    printk(KERN_INFO "%s: Exit char device.\n",THIS_MODULE->name);
    for(i=dev_count-1;i>=0;i--)
    {
        cdev_del(&pchar_array[i].my_dev);
        printk(KERN_INFO "%s: char device %d has been deleted.\n",THIS_MODULE->name,i);
    }
    for(i=dev_count-1;i>=0;i--)
    {
        l_devno=MKDEV(major_no, i);
        device_destroy(pclass,l_devno);
        printk(KERN_INFO "%s: destroy device file %d.\n",THIS_MODULE->name,i);
    }
    class_destroy(pclass);
    printk(KERN_INFO "%s: destroy device class.\n",THIS_MODULE->name);
    unregister_chrdev_region(devno,dev_count);
    printk(KERN_INFO "%s: char device de-allocated.\n",THIS_MODULE->name);
}

module_init(my_char_init);
module_exit(my_char_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin  <abcd@gmail.com>");
MODULE_DESCRIPTION("STANDARD PSEUDO CHAR DEVICE");

