#include <stdio.h>

typedef struct emp {
    int id;         // offset=0
    char name[20];  // offset=4
    int sal;        // offset=24
    char job[12];   // offset=28
}emp_t;

#define offset_of(type,member) ((long)&((type*)0)->member)

int main() {
    emp_t e1 = { .id=1, .name="Nilesh", .sal=60000, .job="Trainer" };
    printf("sizeof(emp_t) = %lu\n", sizeof(emp_t)); // 40
    
    printf("&id = %ld\n", (long)&((emp_t*)0)->id);
    printf("&name = %ld\n", (long)&((emp_t*)0)->name);
    printf("&sal = %ld\n", (long)&((emp_t*)0)->sal);
    printf("&job = %ld\n", (long)&((emp_t*)0)->job);

    printf("&id = %ld\n", offset_of(emp_t, id));
    printf("&name = %ld\n", offset_of(emp_t, name));
    printf("&sal = %ld\n", offset_of(emp_t, sal));
    printf("&job = %ld\n", offset_of(emp_t, job));
    return 0;
}

