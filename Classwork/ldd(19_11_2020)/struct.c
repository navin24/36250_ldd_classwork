#include <stdio.h>

// arr[i] --> i th element of array.
//  *(arr+i) --> pointer arithmetic.

// ptr + i --> ptr + i * sf(ptr).

typedef struct emp {
    int id;         // 4
    char name[20];  // 20
    int sal;        // 4
    char job[12];   // 12
}emp_t;

int main() {
    emp_t e1 = { .id=1, .name="Nilesh", .sal=60000, .job="Trainer" };
    emp_t *p1 = &e1;
    printf("sizeof(emp_t) = %lu\n", sizeof(emp_t)); // 40
    printf("sal = %d, sal = %d\n", e1.sal, p1->sal);
    printf("sal = %d, sal = %d\n", *(int*)((char*)&e1 + 24), *(int*)((char*)p1 + 24));
    printf("base=%lu: &id=%lu, &name=%lu, &sal=%lu, &job=%lu\n", p1, &p1->id, p1->name, &p1->sal, p1->job);
    p1 = NULL;
    printf("base=%lu: &id=%lu, &name=%lu, &sal=%lu, &job=%lu\n", p1, &p1->id, p1->name, &p1->sal, p1->job);
    //base=0, &id=0, &name=4, &sal=24, &job=28
    return 0;
}
