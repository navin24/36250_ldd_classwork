#include <stdio.h>

typedef struct emp {
    int id;         // offset=0
    char name[20];  // offset=4
    int sal;        // offset=24
    char job[12];   // offset=28
}emp_t;

#define offset_of(type,member) ((long)&((type*)0)->member)
#define container_of(ptr,type,member) ((type*)((char*)ptr - offset_of(type,member)))
int main() {
    emp_t *p1, e1 = { .id=1, .name="Nilesh", .sal=60000, .job="Trainer" };
    int *psal = &e1.sal;
    printf("sizeof(emp_t) = %lu\n", sizeof(emp_t)); // 40
    p1 = (emp_t*)((char*)psal - offset_of(emp_t, sal));
    printf("&e1 = %p, p1 = %p\n", &e1, p1);
    printf("%p\n", container_of(&e1.id, emp_t, id));
    printf("%p\n", container_of(&e1.name, emp_t, name));
    printf("%p\n", container_of(&e1.sal, emp_t, sal));
    printf("%p\n", container_of(&e1.job, emp_t, job));
    return 0;
}


/*
container_of(ptr,type,member)
    * macro returns base address of struct variable in which given member is present.
*/