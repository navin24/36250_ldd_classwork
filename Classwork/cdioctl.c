#include <sys/ioctl.h>
#include <linux/cdrom.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define CDROM_DEV "/dev/sr0"

int main() 
{
	int ret, fd = open(CDROM_DEV, O_RDONLY | O_NONBLOCK);
	if(fd < 0) {
			perror("failed to open cdrom");
			exit(1);
			}
		ret = ioctl(fd, CDROMEJECT, 0);
		if(ret != 0) {
				perror("cdrom ioctl() failed");
				exit(1);
				}
		close(fd);
		return 0;
}

// terminal> gcc cdrom.c -o cdrom.out
// // terminal> ./cdrom.out
