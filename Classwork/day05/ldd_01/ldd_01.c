#include <linux/module.h>
#include <linux/fs.h>

#define MAXLEN 4000
static char my_buf[MAXLEN]; // pseudo char device

static dev_t devno;
static int major = 250;

static int __init vdg_init(void) {
	int ret, minor;
	printk(KERN_INFO "vdg_init() called.\n");
	// allocate device number for the device
	devno = MKDEV(major, 0);
	ret = alloc_chrdev_region(&devno, 0, 1, "vdg_char");
	if(ret != 0) {
		printk(KERN_INFO "alloc_chrdev_region() failed.\n");
		return -1;
	}
	major = MAJOR(devno);
	minor = MINOR(devno);
	printk(KERN_INFO "alloc_chrdev_region() allocated major number: %d/%d\n", major, minor);
	// create device class & device file
	// initialize cdev struct & add into kernel
	return 0;
}

static void __exit vdg_exit(void) {
	printk(KERN_INFO "vdg_exit() called.\n");

	unregister_chrdev_region(devno, 1);
	printk(KERN_INFO "unregister_chrdev_region() released device number.\n");
}

module_init(vdg_init);
module_exit(vdg_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vijay D. Gokhale <astromedicomp@yahoo.com>");
MODULE_DESCRIPTION("Pseudo Char Device Driver for Sunbeam DESD Feb 2020.");
