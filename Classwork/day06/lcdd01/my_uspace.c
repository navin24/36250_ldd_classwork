#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>

int main(void)
{
    int fd,ret;

    char buf[512];
    fd = open("/dev/char_dev", O_RDWR);
    if(fd<0)
    {
        perror("failed to open.\n");
        _exit(1);
    }
    ret = write(fd,"Hi Navin\n", 9);
    printf("data  written %d\n",ret);

    ret = write(fd,"Hi Ritu\n", 8);
    printf("data  written %d\n",ret);

    ret = write(fd,"Hi Sweety\n", 10);
    printf("data  written %d\n",ret);

    ret = write(fd,"Bye\n", 4);
    printf("data  written %d\n",ret);

    close(fd);

    fd = open("/dev/char_dev", O_RDWR);
    if(fd<0)
    {
        perror("failed to open.\n");
        _exit(1);
    }

    ret = read(fd,buf,sizeof(buf));
    printf("Number of bytes read    :   %d\n",ret);
    buf[ret] = '\0';
    printf("data is:     %s\n",buf);
    close(fd);

return 0;
}