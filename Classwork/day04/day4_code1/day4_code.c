#include <linux/module.h>
#include<linux/moduleparam.h>

static int cnt = 2;
static char *name = "Kukreja";

module_param(name,charp,0600);
module_param(cnt,int,0600);

static int __init desd_module_init(void)
{
    int i;
    printk("%s: desd_module_init() called.\n",THIS_MODULE->name);
    for(i=0;i<cnt;i++)
    {
        printk("%s: Hi Navin %s.\n",THIS_MODULE->name,name);
    }
    return 0;
}

static void __exit desd_module_exit(void)
{
    int i;
    printk("%s: desd_module_exit() called.\n",THIS_MODULE->name);
    for(i=0;i<cnt;i++)
    {
        printk("%s: Bye Navin %s.\n",THIS_MODULE->name,name);
    }
}

module_init(desd_module_init);
module_exit(desd_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin Kukreja <navin.kukreja@gmail.com>");
MODULE_DESCRIPTION("Standard Hello Kernel Module for DESD Feb 2020 @sunbeam");
