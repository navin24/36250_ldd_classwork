// include headers
#include <linux/module.h>

// module initialization --> called by kernel when module is loaded (insmod cmd)
int init_module(void) 
{
	// printk() - print message in the kernel log.
	// kernel log is visible in user space by command "dmesg".
	printk("%s: Hello Kernel Module: init_module() called.\n", THIS_MODULE->name);
	
	// return 0 indicate, successful module loading.
	// in case of error, this function must return -ve error code.
	return 0;
}

// module cleanup/de-initialization --> called by kernel when module is unloaded (rmmod cmd).
void cleanup_module(void)
{
	printk("%s: Bye Kernel Module: cleanup_module() called.\n", THIS_MODULE->name);
}

// module information
MODULE_INFO(license, "GPL");
MODULE_INFO(author, "Nilesh Ghule <nilesh@sunbeaminfo.com>");
MODULE_INFO(description, "Hello kernel module for DESD Feb2020 batch @ Sunbeam Infotech");


