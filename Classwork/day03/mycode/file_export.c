#include<linux/module.h>

int desd_num= 24;

void desd_fun(void)
{
    printk("Hello DESD, How are You?.\n");
}

static int __init my_init_fun(void)
{
    printk("%s: Hi Init Function.\n",THIS_MODULE->name);
    return 0;
}

static void __exit my_exit_fun(void)
{
    printk("%s: Bye Exit Function.\n",THIS_MODULE->name);
}

EXPORT_SYMBOL(desd_num);
EXPORT_SYMBOL(desd_fun);

module_init(my_init_fun);
module_exit(my_exit_fun);

MODULE_LICENSE("GPL");