#include<linux/module.h>
#include "export.h"

static int __init my_init_fun(void)
{
    printk("%s: Hi Init Function.\n",THIS_MODULE->name);
    printk("%s: My imported number is   :   %d.\n",THIS_MODULE->name , desd_num);
    return 0;
}

static void __exit my_exit_fun(void)
{
    printk("%s: Bye Exit Function.\n",THIS_MODULE->name);
    desd_fun();
}


module_init(my_init_fun);
module_exit(my_exit_fun);
MODULE_LICENSE("GPL");