#include <linux/module.h>
#include "export.h"

static int __init desd_module_init(void) 
{
	printk("%s: desd_module_init() called.\n", THIS_MODULE->name);
	printk("%s: accessing exported variable = %d.\n", THIS_MODULE->name, desd_number);
	desd_number++;
	printk("%s: calling exported function.\n", THIS_MODULE->name);
	desd_function();
	return 0;
}

static void __exit desd_module_exit(void)
{
	printk("%s: desd_module_exit() called.\n", THIS_MODULE->name);
}

module_init(desd_module_init);
module_exit(desd_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nilesh Ghule <nilesh@sunbeaminfo.com>");
MODULE_DESCRIPTION("Kernel Module Importing Symbols for DESD Feb 20 @ Sunbeam");