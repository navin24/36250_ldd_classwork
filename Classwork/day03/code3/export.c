#include <linux/module.h>

int desd_number = 1234;

void desd_function(void) 
{
	printk("%s: desd_function() called and desd_number = %d.\n", THIS_MODULE->name, desd_number);
}

static int __init desd_module_init(void) 
{
	printk("%s: desd_module_init() called.\n", THIS_MODULE->name);
	return 0;
}

static void __exit desd_module_exit(void)
{
	printk("%s: desd_module_exit() called.\n", THIS_MODULE->name);
}

module_init(desd_module_init);
module_exit(desd_module_exit);

EXPORT_SYMBOL(desd_number);
EXPORT_SYMBOL(desd_function);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nilesh Ghule <nilesh@sunbeaminfo.com>");
MODULE_DESCRIPTION("Kernel Module Exporting Symbols for DESD Feb 20 @ Sunbeam");