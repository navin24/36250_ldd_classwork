#include <linux/module.h>

int add_two_numbers(int num1, int num2);
int multiply_two_numbers(int num1, int num2);

static int __init desd_module_init(void) 
{
	int result;
	printk("%s: desd_module_init() called.\n", THIS_MODULE->name);
	result = add_two_numbers(12, 5);
	printk("%s: add_two_numbers() returned %d\n", THIS_MODULE->name, result);
	return 0;
}

static void __exit desd_module_exit(void)
{
	int result;
	printk("%s: desd_module_exit() called.\n", THIS_MODULE->name);
	result = multiply_two_numbers(12, 5);
	printk("%s: multiply_two_numbers() returned %d\n", THIS_MODULE->name, result);
}

module_init(desd_module_init);
module_exit(desd_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin Kukreja <navin.kukreja@gmail.com>");
MODULE_DESCRIPTION("Standard Hello Kernel Module for DESD Feb 2020 @sunbeam");
