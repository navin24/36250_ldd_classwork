#include <linux/module.h>

int add_two_numbers(int num1, int num2) {
	printk("%s: add_two_numbers() called.\n", THIS_MODULE->name);
	return num1 + num2;
}

int multiply_two_numbers(int num1, int num2) {
	printk("%s: multiply_two_numbers() called.\n", THIS_MODULE->name);
	return num1 * num2;
}
