#include <linux/module.h>

static int __init desd_module_init(void)
{
    printk("%s: desd_module_init() called.\n",THIS_MODULE->name);
    return 0;
}

static void __exit desd_module_exit(void)
{
    printk("%s: desd_module_exit() called.\n",THIS_MODULE->name);
}

module_init(desd_module_init);
module_exit(desd_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin Kukreja <navin.kukreja@gmail.com>");
MODULE_DESCRIPTION("Standard Hello Kernel Module for DESD Feb 2020 @sunbeam");
